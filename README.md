# Team 18

## Build instructions
### Requirements:
   * Java 1.8 with support JavaFX
   * Gradle build tool 
### Build:
* Use ```$ gradle build Editor``` to build editor
* Use ```$ gradle build Control``` to build cz.sspbrno.team18.control app

## Run:
* Run using ```$ java -jar nameOfApp.jar```
## JavaFX:
For some reason gradle couldn't find modules

You have to add this path to load JavaFX modules if they are localy on your PC
--module-path V:/JavaFX/javafx-sdk-11.0.2/lib --add-modules=javafx.fxml,