package cz.sspbrno.team18.database.control;

import cz.sspbrno.team18.database.Path.Point;
import cz.sspbrno.team18.database.Path.Polygon;
import cz.sspbrno.team18.database.database.SQLiteDBConnector;
import cz.sspbrno.team18.editor.Interfaces.Controller;
import cz.sspbrno.team18.editor.myEventHandler;
import cz.sspbrno.team18.database.connection.UDPClient;
import javafx.fxml.FXML;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;

public class FlightController implements Controller {
    private UDPClient client;
    private Thread videostream;
    protected SQLiteDBConnector dbConnector;
    public volatile ArrayList<Polygon> polygonArrayList;
    public int lastIndex;
    public  Polygon polygon = null;

    @FXML
    public Text showBattery;

    @FXML
    public Text showTime;

    @FXML
    public Text showSpeed;

    @FXML
    public volatile Text commandline;

    @FXML
    public TextField speedField;

    @FXML
    private MenuButton tvar;

    @FXML
    public void initialize() {
        lastIndex = -1;

        polygonArrayList = new ArrayList<>();
        try {
            //client = new UDPClient();
            dbConnector = new SQLiteDBConnector();
            polygonArrayList.addAll(dbConnector.load());
        } catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Chyba při načítání z databáze! : " + e.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }

        for (Polygon pol: polygonArrayList)
        {
            MenuItem mi = new MenuItem(pol.getName());
            mi.setOnAction(new myEventHandler(this));
            tvar.getItems().add( mi );

        }

        System.out.println("Window initialize successfully");
    }

    @FXML
    public void start() {
        client.sendCommand("command", commandline);
    }

    @FXML
    public void status(){

        new Thread(() -> {
            while (Thread.currentThread().isAlive()) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                client.sendCommand("battery?", showBattery, false);
            }
        }).start();


        client.sendCommand("speed?", showSpeed, false);

        new Thread(() -> {
            while (Thread.currentThread().isAlive()) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                client.sendCommand("time?", showTime, false);
            }
        }).start();
    }

    @FXML
    private void handleonkeytyped(KeyEvent event){
      if(event.getCode().equals(KeyCode.W)) {
            client.sendCommand("forward 75", commandline);
      }else if(event.getCode().equals(KeyCode.S)) {
          client.sendCommand("back 75", commandline);
      }else if(event.getCode().equals(KeyCode.A)) {
          client.sendCommand("left 75", commandline);
      }else if(event.getCode().equals(KeyCode.D)) {
          client.sendCommand("right 75", commandline);
      }else if(event.getCode().equals(KeyCode.E)) {
          client.sendCommand("cw 30", commandline);
      }else if(event.getCode().equals(KeyCode.Q)) {
          client.sendCommand("ccw 30", commandline);
      }else if(event.getCode().equals(KeyCode.CONTROL)) {
          client.sendCommand("down 40", commandline);
      }else if(event.getCode().equals(KeyCode.SHIFT)) {
          client.sendCommand("up 40", commandline);
      }
    }

    @FXML
    private void frontFlip(){
        client.sendCommand("flip f", commandline);
    }

    @FXML
    private void backFlip(){
        client.sendCommand("flip b", commandline);
    }

    @FXML
    private void leftFlip(){
        client.sendCommand("flip l", commandline);
    }

    @FXML
    private void rightFlip(){
        client.sendCommand("flip r", commandline);
    }

    @FXML
    private void setupSpeed(){
        Integer newSpeed = Integer.valueOf(speedField.getText());
        client.sendCommand("speed "+newSpeed.toString(), commandline);
    }

    @FXML
    private void emergency(){
        client.sendCommand("emergency", commandline);
    }

    @FXML
    private void land(){
        client.sendCommand("land", commandline);
    }

    @FXML
    private void startVideo() {
        System.out.println("Started Listening");
        videostream = new Thread(() -> {
           client.sendCommand("streamon", commandline);
           try {
               Runtime.getRuntime().exec("ffmpeg -i udp://0.0.0.0:11111 -f sdl Tello");
           } catch (IOException e) {
               e.printStackTrace();
           }
       });
        videostream.start();
    }

    @FXML
    private void getup(){
        client.sendCommand("takeoff", commandline);
    }

    @FXML
    private void close(){
        try {
            if(videostream != null) {
                client.sendCommand("streamoff", commandline);
                client.close();
                videostream.join();
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getLastIndex() {
        return lastIndex;
    }

    @Override
    public ArrayList<Polygon> getPolygonArrayList() {
        return polygonArrayList;
    }

    @Override
    public Polygon getPolygon() {
        return polygon;
    }

    @Override
    public SQLiteDBConnector getDbConnector() {
        return dbConnector;
    }

    @Override
    public void reDraw() {
        ArrayList<Point> points = new ArrayList<>(polygon);
        System.out.println(points);
        ArrayList<Point> newPoints = new ArrayList<>();
        for (int i = 1; i < points.size(); i++)
        {
            newPoints.add(new Point(points.get(i).getX() - points.get(i-1).getX(), points.get(i).getY() - points.get(i-1).getY()));
        }

        System.out.println(newPoints);
        CommandSender sender = new CommandSender(newPoints);
        sender.setup();
        sender.continuePath();
    }

    @Override
    public void setLastIndex(int lastIndex) {
        this.lastIndex = lastIndex;
    }

    @Override
    public void setPolygon(Polygon polygon) {
        this.polygon = polygon;
    }
}