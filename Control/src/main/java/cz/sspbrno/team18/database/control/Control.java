package cz.sspbrno.team18.database.control;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Control extends Application {

    private Stage primaryStage;
    private FXMLLoader resources;

    @Override
    public void start(Stage primaryStage) throws Exception{
        this.primaryStage = primaryStage;
        initializeVariables();
        showStage();

        primaryStage.show();
        System.out.println("Application started");
    }

    private void initializeVariables(){
       resources = new FXMLLoader(this.getClass().getClassLoader().getResource("flightControl.fxml"));
    }

    private void showStage() throws IOException {
        Parent root = resources.load();

        primaryStage.setTitle("Drone Control");
        primaryStage.setScene(new Scene(root, 600, 600));
    }

    public static void main(String[] args) {
        launch(args);
    }
}
