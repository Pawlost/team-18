package cz.sspbrno.team18.database.control;

import cz.sspbrno.team18.database.Path.Point;
import cz.sspbrno.team18.database.connection.UDPClient;

import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class CommandSender {
    private ArrayList<Point> points;
    private DatagramSocket socket;
    private InetAddress address;

    public CommandSender(ArrayList<Point> points) {
        try {
            this.address = InetAddress.getByName(UDPClient.ADDRESS);
            socket = new DatagramSocket();
            this.points = points;
        } catch (SocketException | UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public void setup() {
        command("command");
        command("takeoff");
    }

    public void command(String msg){
        byte[] buf = msg.getBytes();
        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, UDPClient.PORT);
        try {
            socket.send(packet);
            buf = new byte[20];
            packet = new DatagramPacket(buf, buf.length);
            socket.receive(packet);

            String doneText = new String(packet.getData(), 0, packet.getLength(), StandardCharsets.UTF_8);

            System.out.println(doneText);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void fly(String msg){
        byte[] buf = msg.getBytes();
        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, UDPClient.PORT);
        try {
            socket.send(packet);
            buf = new byte[20];
            packet = new DatagramPacket(buf, buf.length);
            socket.receive(packet);

            String doneText = new String(packet.getData(), 0, packet.getLength(), StandardCharsets.UTF_8);

            System.out.println(doneText);

            continuePath();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void continuePath(){
        if(points.size() > 0) {
            System.out.println("here");
            Point point = points.remove(0);
            System.out.println("go " + (int) point.getX() + " " +(int) point.getY() + " 0 100");
            fly("go " + (int) point.getX() + " " +(int) point.getY() + " 0 100");
        }else{
            fly("land");
        }
    }
}
