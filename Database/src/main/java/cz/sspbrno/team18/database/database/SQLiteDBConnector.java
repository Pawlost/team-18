package cz.sspbrno.team18.database.database;

import cz.sspbrno.team18.database.Constants;
import cz.sspbrno.team18.database.Path.Point;
import cz.sspbrno.team18.database.Path.Polygon;

import java.sql.*;
import java.util.*;

public class SQLiteDBConnector {
    private final static String DATABASE_NAME = "polygon.db";
    private Connection con;
    private Statement statement;

    public SQLiteDBConnector() throws Exception {
        Class.forName("org.sqlite.JDBC");
        con = DriverManager.getConnection("jdbc:sqlite:" + DATABASE_NAME);
        statement = con.createStatement();
        prepareTables();
        System.out.println("Opened database successfully");
    }

    private void prepareTables() throws SQLException {
        statement.executeUpdate("CREATE TABLE IF NOT EXISTS " + Constants.OBJECTS_TABLE.name + " " + Constants.OBJECTS_TABLE.createValues);
        statement.executeUpdate("CREATE TABLE IF NOT EXISTS " + Constants.POINTS_TABLE.name + " " + Constants.POINTS_TABLE.createValues);
    }

    public void save(ArrayList<Polygon> objects) throws SQLException {
        if(!objects.isEmpty())
            dropAll();
        for (Polygon polygon : objects)
        {
            String sql = "INSERT INTO " + Constants.OBJECTS_TABLE.name + Constants.OBJECTS_TABLE.normalValues + " " +
                    "VALUES ('" + polygon.getName() + "');";
            statement.executeUpdate(sql);

            sql = "SELECT MAX(ID) AS ID FROM "+ Constants.OBJECTS_TABLE.name;
            ResultSet rs = statement.executeQuery(sql);
            int id = rs.getInt("ID");

            for (int i = 0; i < polygon.size(); i++) {
                sql = "INSERT INTO " + Constants.POINTS_TABLE.name + Constants.POINTS_TABLE.normalValues +
                        "VALUES (" + polygon.get(i).getX() + ", " + polygon.get(i).getY() + ", "+ id +");";
                statement.executeUpdate(sql);
            }
        }
    }

    public ArrayList<Polygon> load() throws SQLException {
        ArrayList<Polygon> polygons = new ArrayList<>();
        String sql = "SELECT * FROM '" + Constants.POINTS_TABLE.name + "' INNER JOIN " + Constants.OBJECTS_TABLE.name + " ON " + Constants.OBJECTS_TABLE.name + ".ID=" + Constants.POINTS_TABLE.name + ".OBJECTID;";
        ResultSet objectResults = statement.executeQuery(sql);

        String name = "";
        Polygon polygon = null;
        while (objectResults.next()) {
            float x = objectResults.getFloat("X");
            float y = objectResults.getFloat("Y");
            if(!objectResults.getString("NAME").equals(name)) {
                name = objectResults.getString("NAME");
                polygon = new Polygon(name);
                polygons.add(polygon);
            }

            Point point = new Point(x, y);

            Objects.requireNonNull(polygon).add(point);
        }
        System.out.println("HERE");
        return polygons;
    }

    public boolean dropAll() throws SQLException
    {
        boolean result = true;
        String sql = "DELETE FROM "+ Constants.POINTS_TABLE.name;
        result = statement.execute(sql);
        sql = "DELETE FROM " + Constants.OBJECTS_TABLE.name;
        result &= statement.execute(sql);
        return result;
    }

    public void close() throws SQLException {
        statement.close();
        con.close();
    }
}
