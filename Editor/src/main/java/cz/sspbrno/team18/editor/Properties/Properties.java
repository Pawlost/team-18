package cz.sspbrno.team18.editor.Properties;
import java.io.*;
import java.util.HashMap;

public class Properties
{
    private File file;
    private HashMap<String,String> properties;

    public Properties(File file) throws IOException
    {
        this.properties = new HashMap<>();
        this.file = file;
        loadFromFile();
    }

    /**
     * Loads properties file
     * @throws IOException
     */
    private void loadFromFile() throws IOException
    {
        if(!file.exists())
        {
            new File(file.getParent()).mkdirs();
            file.createNewFile();
        }

        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        while (( line = br.readLine()) != null)
        {
            this.properties.put(line.split(";")[0],line.split(";")[1]);
        }
    }

    /**
     * Getting property
     * @param key
     * @return
     */
    public String getProperty(String key)
    {
        return this.properties.get(key);
    }

    /**
     * Setting property
     * @param key
     * @param value
     * @throws IOException
     */
    public void setProperty(String key, String value) throws IOException
    {
        this.properties.put(key, value);
        saveToFile();
    }

    /**
     * Saving properties to file
     * @throws IOException
     */
    private void saveToFile() throws IOException
    {
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));

        for (String key:this.properties.keySet())
        {
            bw.write(key+";"+this.properties.get(key));
            bw.newLine();
        }

        bw.flush();
    }
}
