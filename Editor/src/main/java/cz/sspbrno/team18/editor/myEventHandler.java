package cz.sspbrno.team18.editor;

import cz.sspbrno.team18.database.Path.Polygon;
import cz.sspbrno.team18.editor.Interfaces.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;


public class myEventHandler implements EventHandler<ActionEvent>
{
    private Controller editorController;
    public myEventHandler(Controller editorController)
    {
        this.editorController = editorController;
    }

    @Override
    public void handle(ActionEvent event)
    {
        String nameOfCaller = ((MenuItem)event.getSource()).getText();
        editorController.setLastIndex(editorController.getPolygonArrayList().indexOf(editorController.getPolygon()));
        if(editorController.getLastIndex() == -1 && !(editorController.getPolygon() == null))
        {
            editorController.getPolygonArrayList().add(editorController.getPolygon());
            editorController.setLastIndex(editorController.getPolygonArrayList().indexOf(editorController.getPolygon()));
        }

        editorController.setPolygon(getPolygonByName(nameOfCaller));
        editorController.reDraw();
    }

    private Polygon getPolygonByName(String name)
    {
        for (Polygon pt:editorController.getPolygonArrayList())
        {
            if(pt.getName().equals(name))
                return pt;
        }
        return null;
    }
}

