package cz.sspbrno.team18.editor;

import cz.sspbrno.team18.database.Path.Point;
import cz.sspbrno.team18.database.Path.Polygon;

public class Utils
{
    public static Polygon calculateToDraw(Polygon oldPolygon, double canvasWidth, double canvasHeight)
    {
        Polygon newPolygon = new Polygon();
        for (Point pt: oldPolygon)
        {
            newPolygon.push(new Point(pt.getX() + (float)canvasWidth / 2 ,-pt.getY() + (float)canvasHeight / 2));
        }
        return newPolygon;
    }

    public static Point calculateToSave(Point pt, double canvasWidth, double canvasHeight)
    {
        Point newPolygon = new Point(pt.getX() - (float)canvasWidth / 2 ,( -pt.getY() + (float)canvasHeight / 2));
        return newPolygon;
    }
}
