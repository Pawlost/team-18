package cz.sspbrno.team18.editor;

import cz.sspbrno.team18.editor.Interfaces.Controller;
import cz.sspbrno.team18.database.database.SQLiteDBConnector;
import cz.sspbrno.team18.database.Path.Point;
import cz.sspbrno.team18.database.Path.Polygon;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import javax.swing.*;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class EditorController implements Initializable, Controller
{
    protected SQLiteDBConnector dbConnector;
    public  ArrayList<Polygon> polygonArrayList;
    public  int lastIndex;
    public  Polygon polygon = null;
    @FXML
    private TextField xPos;
    @FXML
    private TextField yPos;
    @FXML
    public TextField name;
    @FXML
    private Canvas canvas;
    @FXML
    private MenuButton tvar;
    @FXML
    private MenuButton SQLSelect;
    @FXML
    private Button complete;
    @FXML
    private Button addPoint;
    @FXML
    private Button send;
    @FXML
    private Button newt;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        System.out.println(Editor.jmlsp);
        setButtonsNames();
        lastIndex = -1;

        polygonArrayList = new ArrayList<>();
        try
        {
            dbConnector = new SQLiteDBConnector();
            polygonArrayList.addAll(dbConnector.load());
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null,
                    Editor.jmlsp.getByID("Editor.DatabaseLoadError") + e.getMessage(),
                    Editor.jmlsp.getByID("Editor.Error"), JOptionPane.ERROR_MESSAGE);
        }

        for (Polygon pol: polygonArrayList)
        {
            MenuItem mi = new MenuItem(pol.getName());
            mi.setOnAction(new myEventHandler(this));
            tvar.getItems().add( mi );
        }
        reDraw();

        System.out.println("Window initialize successfully");
    }

    private void setButtonsNames()
    {
        complete.setText(Editor.jmlsp.getByID("Editor.Complete"));
        addPoint.setText(Editor.jmlsp.getByID("Editor.AddPoint"));
        send.setText(Editor.jmlsp.getByID("Editor.Send"));
        newt.setText(Editor.jmlsp.getByID("Editor.New"));
        SQLSelect.setText(Editor.jmlsp.getByID("Editor.SQLSelect"));
        name.setPromptText(Editor.jmlsp.getByID("Editor.Name"));
        tvar.setText(Editor.jmlsp.getByID("Editor.Polygon"));

    }

    /**
     * Draws Axes in the polygon
     */
    private void drawAxes()
    {
        double width = canvas.getWidth();
        double height = canvas.getHeight();

        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.setStroke(Color.WHITE);

        graphicsContext.setLineWidth(1.0);
        graphicsContext.setStroke(Color.BLACK);

        graphicsContext.beginPath();
        graphicsContext.moveTo(width/2,0);
        graphicsContext.lineTo(width/2,height);
        graphicsContext.stroke();

        graphicsContext.moveTo(0,height/2);
        graphicsContext.lineTo(width,height/2);
        graphicsContext.stroke();
    }

    /**
     * Adding new point to actual polygon
     * @param mouseEvent
     */
    public void onClickedAddPoint(MouseEvent mouseEvent)
    {
        addPoint();
    }

    /**
     * This function will insert data to the database
     * @param mouseEvent
     */
    public void onClickedSendSql(MouseEvent mouseEvent) throws SQLException
    {
        onClickedNew(mouseEvent);
        dbConnector.save(polygonArrayList);
    }

    @Override
    public int getLastIndex() {
        return lastIndex;
    }

    @Override
    public ArrayList<Polygon> getPolygonArrayList() {
        return polygonArrayList;
    }

    @Override
    public Polygon getPolygon() {
        return polygon;
    }

    @Override
    public SQLiteDBConnector getDbConnector() {
        return dbConnector;
    }

    /**
     * Draw the polygon on the canvas
     */
    @Override
    public void reDraw()
    {
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.setLineWidth(1.0);

        graphicsContext.setStroke(Color.WHITE);

        graphicsContext.setFill(Color.gray(0.90));
        graphicsContext.fillRect(0,0,canvas.getWidth(),canvas.getHeight());

        if(lastIndex != polygonArrayList.indexOf(polygon) && lastIndex != -1)
        {
            lastIndex = polygonArrayList.indexOf(polygon);
        }

        graphicsContext.setStroke(Color.BLACK);

        drawAxes();

        if(polygon == null && !polygonArrayList.isEmpty())
            return;

        drawPolygon(graphicsContext);
    }


    private void drawPolygon(GraphicsContext graphicsContext)
    {
        for (int i = 1; i < polygon.size(); i++)
        {
            graphicsContext.beginPath();

            Point lastPoint = Utils.calculateToDraw(polygon, canvas.getWidth(), canvas.getHeight()).get(i-1);
            Point nowPoint = Utils.calculateToDraw(polygon, canvas.getWidth(), canvas.getHeight()).get(i);

            graphicsContext.moveTo(lastPoint.getX(), lastPoint.getY());
            graphicsContext.lineTo(nowPoint.getX(), nowPoint.getY());

            graphicsContext.stroke();
        }
    }

    /**
     * Complete the pattern (set last point to x = 0, y = 0
     * @param mouseEvent
     */
    public void onClickedComplete(MouseEvent mouseEvent)
    {
        if (polygon != null)
        {
            if (polygon.size() > 2)
            {
                polygon.add(new Point(0, 0));
                reDraw();
            }
        }
        else
        {
            JOptionPane.showMessageDialog(null, Editor.jmlsp.getByID("Editor.CompleteError"),
                    Editor.jmlsp.getByID("Editor.Error"),JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Creates new polygon
     * @param mouseEvent
     * @throws SQLException
     */
    public void onClickedNew(MouseEvent mouseEvent) throws SQLException
    {
        if (polygon == null)
            return;

        polygon.setName(name.getText());
        if(name.getText().equals(""))
            polygon.setName("Default polygon");

        String[] options = {Editor.jmlsp.getByID("Global.Yes").toString(), Editor.jmlsp.getByID("Global.No").toString()};

        if (JOptionPane.showOptionDialog(null, Editor.jmlsp.getByID("Editor.SaveChanges"),
                Editor.jmlsp.getByID("Global.Option"), 0,1,null, options,"yes") == 0)
        {
            polygonArrayList.add(polygon);
            MenuItem mi = new MenuItem(polygon.getName());
            mi.setOnAction(new myEventHandler(this));
            tvar.getItems().add(mi);
        }

        polygon = null;
        reDraw();
    }

    /**
     * Reserved for future use
     * @param mouseEvent
     */
    public void onClickedCanvas(MouseEvent mouseEvent)
    {
        if (polygon == null)
        {
            if (!name.getText().equals(""))
                polygon = new Polygon(name.getText());
            else
                polygon = new Polygon("Default Name");
        }

        Editor.addNextToStack(polygon, Utils.calculateToSave(new Point((float)mouseEvent.getX(),(float)mouseEvent.getY()),
                canvas.getWidth(),canvas.getHeight())); //add try catch(ParseExeption)

        reDraw();
    }

    /**
     * Just for debugging
     * @param mouseEvent
     * @throws SQLException
     */
    public void onClickDebug(MouseEvent mouseEvent) throws SQLException
    {
        System.err.println("DEBUG: "+ dbConnector.load());
        reDraw();
    }

    /**
     * On press enter
     * @param keyEvent
     */
    public void onKeyPressedY(KeyEvent keyEvent)
    {
        if(keyEvent.getCode() == KeyCode.ENTER)
        {
            addPoint();
        }
    }

    /**
     * Adding point to actual polygon
     */
    private void addPoint()
    {
        if(polygon == null)
        {
            if (!name.getText().equals(""))
                polygon = new Polygon(name.getText());
            else
                polygon = new Polygon("Default Name");
        }

        try
        {
            Editor.addNextToStack(polygon, new Point(Float.parseFloat(xPos.getText()), Float.parseFloat(yPos.getText())));
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null,
                    Editor.jmlsp.getByID("Editor.BadCords")+": " + e.getMessage(),
                    Editor.jmlsp.getByID("Editor.Error"), JOptionPane.ERROR_MESSAGE);
        }

        reDraw();
    }
    @Override
    public void setLastIndex(int lastIndex) {
        this.lastIndex = lastIndex;
    }

    public void setPolygon(Polygon polygon) {
        this.polygon = polygon;
    }

}
