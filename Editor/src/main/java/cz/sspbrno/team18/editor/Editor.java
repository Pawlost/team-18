package cz.sspbrno.team18.editor;

import cz.sspbrno.team18.editor.JMLSP.JMLSP;
import cz.sspbrno.team18.database.Path.Point;
import cz.sspbrno.team18.database.Path.Polygon;
import cz.sspbrno.team18.editor.Properties.Properties;
import cz.sspbrno.team18.editor.Interfaces.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Editor extends Application
{
    static JMLSP jmlsp = null;
    private static Properties properties = null;
    public static void main(String[] args) throws IOException
    {
        setupJMLSP();
        setupProperties();
        Object[] options = jmlsp.getLanguages().toArray();
        jmlsp.setLanguage((String)options[JOptionPane.showOptionDialog(null,"Select language"
                ,"Language", 1,1,null, options,"en_us.lang")]);

        launch(args); // Starts GUI of the Application

        /*Polygon pointStack;

        pointStack = inputPoints();

        for(Point point: pointStack)
        {
            System.out.println(point);
        }*/

    }

    private static void setupJMLSP() throws IOException
    {
        if(System.getProperty("os.name").contains("Linux"))
            jmlsp = new JMLSP(System.getProperty("user.home") + "/.team-18/jmlsp","en_us.lang");
        else if (System.getProperty("os.name").contains("Win"))
            jmlsp = new JMLSP("%appdata%/team-18/jmlsp","en_us.lang");
    }

    public static void setupProperties() throws IOException
    {
        if(System.getProperty("os.name").contains("Linux"))
            properties = new Properties(new File(System.getProperty("user.home") + "/.team-18/Editor.properties"));
        else if (System.getProperty("os.name").contains("Win"))
            properties = new Properties(new File("%appdata%/team-18/Editor.properties"));

    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        FXMLLoader fxmlLoader = new FXMLLoader();

       // System.out.println(this.getClass().getClassLoader().getResource("Editor.fxml") );
        Parent root = fxmlLoader.load(Objects.requireNonNull(this.getClass().getClassLoader().getResource("Editor.fxml")).openStream()); //ehm... don't mind about it, it works :D

        Scene scene = new Scene(root);

        Controller editorConroller = fxmlLoader.getController();

        primaryStage.setResizable(false);
      /*  primaryStage.setOnCloseRequest(event -> {
            try {
                editorConroller.getDbConnector().close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });*/

        try
        {
            primaryStage.setHeight(Double.parseDouble(Editor.properties.getProperty("Height")));
            primaryStage.setWidth(Double.parseDouble(Editor.properties.getProperty("Width")));
        }
        catch (Exception e)
        {
            Editor.properties.setProperty("Height","720");
            Editor.properties.setProperty("Width","1280");
        }

        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setTitle("Team-18 Editor");
    }

    /**
     * Adding points from terminal
     * @return Stack of input Points
     */

    public static Polygon inputPoints()
    {
        Scanner scn = new Scanner(System.in);
        System.out.println("Zadej nazev polygonu: ");
        Polygon points = new Polygon(scn.nextLine());

        do
        {
            System.out.print("Zadej souřadnice bodu(q - pro ukonceni zadavani, X;Y - tvar zadavani): ");
            String input = scn.nextLine();

            if(input.equals("q"))
                break;

            String[] splitedArray = input.split(";");

            try
            {
                if(!addNextToStack(points, new Point(Float.parseFloat(splitedArray[0]), Float.parseFloat(splitedArray[1]))))
                    throw new Exception("addPointExecption");
            }
            catch (Exception e)
            {
                System.err.println("Chyba ve tvaru zadaného čísla: "+ e.getMessage());
            }
        } while (true);
        return points;
    }

    /**
     * Calls calculateRandomPoints with multiplier = 100
     * @param count - Number of points what will be generated
     * @return - Stack of generated points
     */
    public static Polygon calculateRandomPoints(int count)
    {
        return calculateRandomPoints(count, 100f);
    }

    /**
     * Generate random points
     * @param count - Number of points what will be generated
     * @param multiplier - Numbers will be multiplied by this number
     * @return Stack of random Points
     */
    public static Polygon calculateRandomPoints(int count, float multiplier)
    {
        ThreadLocalRandom random = ThreadLocalRandom.current();

        Polygon points = new Polygon("Random polygon" + random.nextInt());

        for(int i =0; i < count; i++)
        {
            Point point = new Point(random.nextFloat() * multiplier, random.nextFloat() * multiplier);
            addNextToStack(points, point);
        }

        return points;
    }

    /**
     * Adding point to the stack safely
     * @param stack - Stack of point, can't be null
     * @param nextPoint - Next point in the stack can't be same as the last point
     * @return true if success, false if failure
     */
    public static boolean addNextToStack(Polygon stack, Point nextPoint)
    {
        if(stack == null)
            return false;

        if(stack.size() == 0)
        {
            stack.push(nextPoint);
            return true;
        }
        Point lastPoint = stack.pop();
        stack.push(lastPoint);

        if(nextPoint.equals(lastPoint))
            return false;

        stack.push(nextPoint);

        return true;
    }


}
